use inflate_nostd::inflate_bytes;

#[test]
fn sample_example() {
    let encoded = [203, 72, 205, 201, 201, 87, 40, 207, 47, 202, 73, 1, 0];
    let decoded = inflate_bytes(&encoded[..]).unwrap();
    assert_eq!(decoded, "hello world".as_bytes());
}

#[test]
fn sample_example2() {
    let encoded = [203, 72, 205, 201, 201, 87, 40, 207, 47, 202, 73, 1, 0];
    let decoded = inflate_bytes(&encoded[..]);
    assert_eq!(decoded.unwrap(), "hello world".as_bytes());
}

#[test]
fn another_sample_example() {
    let encoded = [
        203, 201, 207, 207, 86, 200, 205, 207, 213, 81, 40, 207, 44, 201, 200, 47, 45, 81, 40, 46,
        73, 1, 0,
    ];
    let decoded = inflate_bytes(&encoded[..]);
    assert_eq!(decoded.unwrap(), "look mom, without std".as_bytes());
}
