# Inflate_nostd

## Inflate it's an algorithm to uncompress data compressed deflated

This is a very modified version of the [image-rs/inflate](https://github.com/image-rs/inflate) removing the std dependency and minimizing Heap usage (although the usage was reduced quite a lot, I have not managed to make it 100% stack).  

# WARNING!!
This crate use unsafe code.
In particular I use some buffers stored in `static mut` and cause conflict when you try to use this crate in multithread.
If you want to uncompress data in multithread use the [image-rs/inflate crate](https://github.com/image-rs/inflate).

## Usage
```
use inflate_nostd::inflate_bytes;

let encoded = [203, 72, 205, 201, 201, 87, 40, 207, 47, 202, 73, 1, 0];
let decoded = inflate_bytes(&encoded[..]).unwrap();
println!("{}", core::str::from_utf8(&decoded).unwrap());
```

## License
The code from image-rs/inflate is under the license MIT License and this another crate it's heavy inspired on that, so, this code is released under the MIT license.
